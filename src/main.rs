use std::env;
use plotly::{Plot, Bar};
use plotly::layout::{Axis, Layout};
use plotly::common::Title;

#[derive(Debug)]
struct Location {
    lat: f32,
    lon: f32,
    name: String,
}

struct LocationDB {
    locations: Vec<Location>,
}

impl LocationDB {
    fn new() -> LocationDB {
        LocationDB { locations: Vec::new() }
    }

    fn add_location(&mut self, lat: f32, lon: f32, name: &str) {
        let location = Location {
            lat,
            lon,
            name: name.to_string(),
        };
        self.locations.push(location);
    }

    fn haversine_distance(a: &Location, b: &Location) -> f64 {
        let radius = 6371.0; // Earth's radius in kilometers
        let lat1_rad = a.lat.to_radians();
        let lat2_rad = b.lat.to_radians();
        let lon1_rad = a.lon.to_radians();
        let lon2_rad = b.lon.to_radians();

        let delta_lat = lat2_rad - lat1_rad;
        let delta_lon = lon2_rad - lon1_rad;

        let a = (delta_lat / 2.0).sin().powi(2)
            + lat1_rad.cos() * lat2_rad.cos() * (delta_lon / 2.0).sin().powi(2);
        let c = 2.0 * a.sqrt().atan2((1.0 - a).sqrt());

        (radius as f64) * (c as f64) // Distance in kilometers
    }

    fn find_distances(&self, city_name: &str) -> Vec<(&Location, f64)> {
        let query_location = self.locations.iter().find(|&loc| loc.name == city_name);
        match query_location {
            Some(query_loc) => {
                let mut distances: Vec<(&Location, f64)> = self.locations.iter().map(|loc| {
                    let distance = LocationDB::haversine_distance(&query_loc, loc);
                    (loc, distance)
                }).collect();
                
                distances.sort_by(|(_, dist_a), (_, dist_b)| dist_a.partial_cmp(&dist_b).unwrap());
                distances
            },
            None => {
                println!("City not found.");
                vec![]
            }
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        eprintln!("Usage: cargo run -- \"<location name>\"");
        return;
    }

    let city_name = &args[1];

    let mut db = LocationDB::new();
    db.add_location(32.7157, -117.1611, "San Diego");
    db.add_location(37.7749, -122.4194, "San Francisco");
    db.add_location(34.0522, -118.2437, "Los Angeles");
    db.add_location(41.8781, -87.6298, "Chicago");
    db.add_location(40.7128, -74.0060, "New York");
    db.add_location(29.7604, -95.3698, "Houston");

    println!("Results:");
    println!("Distances from {}:", city_name);
    let distances = db.find_distances(city_name);
    for (loc, distance) in &distances {
        println!("{}: {:.2} kilometers", loc.name, distance);
    }

    // Visualize the output
    let mut plot = Plot::new();
    let cities: Vec<String> = distances.iter().map(|(loc, _)| loc.name.clone()).collect();
    let distances: Vec<f64> = distances.iter().map(|(_, dist)| *dist).collect();
    let trace = Bar::new(cities, distances).name("Distances");
    
    let layout = Layout::new()
        .x_axis(Axis::new().title(Title::new("City")))
        .y_axis(Axis::new().title(Title::new("Distance (km)")));

    plot.add_trace(trace);
    plot.set_layout(layout);

    plot.show();
}
