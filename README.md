# Mini projrct 7

Data Processing with Vector Database

## Requiements

Ingest data into Vector database

Perform queries and aggregations

Visualize output

## Steps

1. Create a rust project.

```
   cargo new mini-project7
```

2. Write code in src/main.rs. I use Qdrant for vector database. The code contain the following steps.

   (1) Adding datas. These datas are some citys and their locations.

   (2) Perform queries. Input a city's name, it will ouput other citys' distance from the input city in ascent sort.

   (3) Visualize output. Use plotly visualize the output in json format, the bar plot is shown in below.
3. Build, and run the project.

```
cargo build
cargo run -- "Chicago"
```

### Screenshot display

![img](sc.png)

![img](visual.png)
